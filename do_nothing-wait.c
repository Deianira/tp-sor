#include <stdio.h>  //incluimos la libreria de estandar input/output
#include <unistd.h> //para hacer sleep
#include <stdlib.h> //para libreria de numeros random: srand, rand 
#include <time.h>   //para inicializar el tiempo
#include <sys/types.h>
#include <sys/wait.h>

void do_nothing(int microseconds, char* mensaje){
  usleep(microseconds); //dormir el thread, simula que esta haciendo alguna tarea
  printf("\n %s \n",mensaje);   
}

void do_nothing_random(char* mensaje){
  srand(time(NULL));                    //inicializar la semilla del generador random:
  int microseconds = rand() % 1000 + 1; //generar un numer random entre 1 y 1000:
  usleep(microseconds);                 //dormir el thread, simula que esta haciendo alguna tarea
  printf("\n %s \n",mensaje);   
}


int main() {
  char* msg= "hola";
  pid_t pid;
  int estado;
  pid = fork();
  if (pid < 0){
	fprintf(stderr, "Fallo en el fork");
	return 1;
  }
  else if (pid == 0){ //Es proceso hijo
	do_nothing(2000000,msg); //esperar 2 segundos, 1 millon de microsegundos es 1 segundo 
  }
  else{ //Es proceso padre
	wait(&estado);
  	do_nothing_random(msg);  //esperar un tiempo random antes de imprimir el mensaje
  }
  return 0;
}

//para compilar: gcc do_nothing.c -o ejecutable
//para ejecutar: ./ejecutable

 
