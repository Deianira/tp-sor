#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#include <time.h>

sem_t uno, dos, tre, cua, cin, sei, sie;

int cont=0;
int cantVeces;

void* thread1(void* arg){
	while(cont<(cantVeces*7)){ //*7 porque son 7 threads por vuelta
		cont++;
		sem_wait(&uno);
		printf("Pienso\n");
		sem_post(&dos);
	}
}
void* thread2(void* arg){
	while(cont<(cantVeces*7)){
		cont++;
		sem_wait(&dos);
		printf("Mientras lavo los platos, ");
		sem_post(&tre);
	}
}
void* thread3(void* arg){
	while(cont<(cantVeces*7)){
		cont++;
		sem_wait(&tre);
		printf(" mientras limpio el piso, ");
		sem_post(&cua);
	}
}
void* thread4(void* arg){
	while(cont<(cantVeces*7)){
		cont++;
		sem_wait(&cua);
		printf(" mientras riego las plantas.");
		sem_post(&cin);
	}
}
void* thread5(void* arg){
	while(cont<(cantVeces*7)){
		cont++;
		sem_wait(&cin);
		printf("\nExisto!\n");
		sem_post(&sei);
	}
}
void* thread6(void* arg){
	while(cont<(cantVeces*7)){
		cont++;
		sem_wait(&sei);
		printf("Hablar, ");
		sem_post(&sie);
	}
}
void* thread7(void* arg){
	while(cont<(cantVeces*7)){
		cont++;
		sem_wait(&sie);
		printf("tomar una decisión.\n");
		sem_post(&uno);
	}
}

int main(){
	printf ("Ingrese la cantidad de veces que desea repetir la secuencia. \n");
	scanf("%d",&cantVeces);	

	pthread_t t1, t2, t3, t4, t5, t6, t7;
	sem_init(&uno,0,1);
	sem_init(&dos,0,0);
	sem_init(&tre,0,0);
	sem_init(&cua,0,0);
	sem_init(&cin,0,0);
	sem_init(&sei,0,0);
	sem_init(&sie,0,0);

	pthread_create(&t1, NULL, *thread1, NULL);
	pthread_create(&t2, NULL, *thread2, NULL);
	pthread_create(&t3, NULL, *thread3, NULL);
	pthread_create(&t4, NULL, *thread4, NULL);
	pthread_create(&t5, NULL, *thread5, NULL);
	pthread_create(&t6, NULL, *thread6, NULL);
	pthread_create(&t7, NULL, *thread7, NULL);

	pthread_join(t1, NULL);
	pthread_join(t2, NULL);
	pthread_join(t3, NULL);
	pthread_join(t4, NULL);
	pthread_join(t5, NULL);
	pthread_join(t6, NULL);
	pthread_join(t7, NULL);

	sem_destroy(&uno);
	sem_destroy(&dos);
	sem_destroy(&tre);
	sem_destroy(&cua);
	sem_destroy(&cin);
	sem_destroy(&sei);
	sem_destroy(&sie);

	pthread_exit(NULL);
	return 0;
}
